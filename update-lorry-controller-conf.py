#!/usr/bin/env python3

from collections import OrderedDict
from pathlib import Path
import json

dir_path = Path(__file__).absolute().parent

subdirs = sorted([d for d in dir_path.iterdir() if d.is_dir()])

dirs_to_ignore = [
    ".git",
]

all_groups = []
for subdir in subdirs:
    if subdir.name in dirs_to_ignore:
        continue
    group = subdir.name

    lorry_files = [f for f in subdir.iterdir() if f.is_file() and f.suffix == ".lorry"]
    if not lorry_files:
        continue

    lc_conf_entry = OrderedDict(
        {
            "type": "lorries",
            "interval": "3H",
            "lorry-timeout": 7200,
            "prefix": f"Infrastructure/Mirrors/lorry-mirrors/{ group }",
            "globs": [f"{ group }/*.lorry"],
        }
    )
    all_groups.append(lc_conf_entry)


lc_conf_path = Path(dir_path, "lorry-controller.conf")
with open(lc_conf_path, "w") as lc_conf_file:
    json.dump(all_groups, lc_conf_file, indent=2)
    lc_conf_file.write("\n")
