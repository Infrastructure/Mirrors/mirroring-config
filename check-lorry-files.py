#!/usr/bin/env python3

from pathlib import Path

from lorrycontroller import readconf

dir_path = Path(__file__).absolute().parent

subdirs = [d for d in dir_path.iterdir() if d.is_dir()]

dirs_to_ignore = [
    ".git",
]

files_to_check = []
for subdir in subdirs:
    if subdir.name in dirs_to_ignore:
        continue

    lorry_files = [f for f in subdir.iterdir() if f.is_file() and f.suffix == ".lorry"]

    files_to_check.extend(lorry_files)

rc = readconf.ReadConfiguration({}, "")
error = False
for lorry_file in files_to_check:
    f = rc.get_valid_lorry_specs(Path(dir_path, lorry_file))
    # if parsed file is empty, there was probably an error
    if not f:
        error = True

if error:
    exit(1)
